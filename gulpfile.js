const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const uglify = require('gulp-uglify');
const pump = require('pump');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const clean = require('gulp-clean');
const gulpSequence = require('gulp-sequence');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');

gulp.task('clean-dist', function(){
    return gulp.src('./dist', {read: false})
        .pipe(clean());
});
gulp.task('sass', function () {
  return gulp.src('./src/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});


gulp.task('copy-html', function(){
    return gulp.src('./src/**/*.html')
               .pipe(gulp.dest('./dist'));
});

gulp.task('concat-js', function() {
    return gulp.src(['./src/js/**/*.js','!./src/js/all.js'])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./src/js/'));
});

gulp.task('transpile', ['concat-js'], () =>
    gulp.src('./src/js/all.js')
        .pipe(babel({presets: ["@babel/env"]}))
        .pipe(gulp.dest('./src/js'))
);

gulp.task('compress', ['transpile'], function (cb) {
    pump([
            gulp.src('./src/js/all.js'),
            uglify(),
            rename("/all.min.js"),
            gulp.dest('src/js/')
        ],
        cb
    );
});

gulp.task('copy-js', ['compress'], function(){
    return gulp.src('./src/js/all.min.js')
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('serve', function(){
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch('./src/**/*.html',['copy-html']).on('change',browserSync.reload);
});

gulp.task('dev', gulpSequence('clean-dist', ['copy-html','copy-js'], 'serve'));